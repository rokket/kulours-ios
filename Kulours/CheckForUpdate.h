//
//  CheckForUpdate.h
//  
//
//  Created by Hassan Mahmood on 02/02/2013.
//
//

#import <Foundation/Foundation.h>
#import "infoViewController.h"

@class infoViewController;
@interface CheckForUpdate : NSObject <UIAlertViewDelegate>{
    infoViewController *info;
}

+ (void)version;
+(void)backgroundColor;
@end
