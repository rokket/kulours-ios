//
//  AppDelegate.h
//  Kulours
//
//  Created by Hassan Mahmood on 07/05/2013.
//  Copyright (c) 2013 Hassan Mahmood. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>{

}



@property (strong, nonatomic) UIWindow *window;

@end
