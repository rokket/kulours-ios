//
//  ViewController.m
//  Kulours
//
//  Created by Hassan Mahmood on 07/05/2013.
//  Copyright (c) 2013 Hassan Mahmood. All rights reserved.
//

#import "ViewController.h"
//#import "TPKeyboardAvoidingScrollView.h"
#import "RSBrightnessSlider.h"
#import "KGStatusBar.h"
#import <QuartzCore/QuartzCore.h>
#import "infoViewController.h"
CGFloat animatedDistance;
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

#define MAX_LENGTH 6
@interface ViewController ()

@end

@implementation ViewController
@synthesize labelHexCode, labelHexText, textHex, btnGenerate, scrollView1, colorPatch, colorPicker, brightnessSlider, keyboardView, primaryShadeView, primaryView, secondaryView, codeLabel, uicolorLabel;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    secondaryView.layer.shadowColor = [UIColor blackColor].CGColor;
    secondaryView.layer.shadowOffset = CGSizeMake(0, 0);
    secondaryView.layer.shadowOpacity = 1;
    secondaryView.layer.shadowRadius = 1.0;
    
    keyboardView.layer.shadowColor = [UIColor blackColor].CGColor;
    keyboardView.layer.shadowOffset = CGSizeMake(0, 0);
    keyboardView.layer.shadowOpacity = 1;
    keyboardView.layer.shadowRadius = 1.0;
    
    
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    
    if (iOSDeviceScreenSize.height == 480){
        
        CGRect frame = keyboardView.frame;
        frame.origin.y = 480;
        frame.origin.x = 0;
        keyboardView.frame = frame;
    }
    
    if (iOSDeviceScreenSize.height == 568){
        CGRect frame = keyboardView.frame;
        frame.origin.y = 568;
        frame.origin.x = 0;
        keyboardView.frame = frame;
        
    }


    
    textHex.delegate = self;

    
    keyOpen = NO;
    
    

    BOOL useArchivedColorPicker = NO;
    savePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/save.dat"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:savePath] && useArchivedColorPicker) {
        colorPicker = [NSKeyedUnarchiver unarchiveObjectWithFile:savePath];
        //colorPicker.frame = CGRectMake(10.0, 20.0, 300.0, 300.0);
    }
    
    [colorPicker setCropToCircle:YES]; // Defaults to YES (and you can set BG color)
	[colorPicker setDelegate:self];
    colorPicker.backgroundColor = [UIColor clearColor];
    
    if (iOSDeviceScreenSize.height == 480){
        //PREPARE NEW VIEW AND PUSH
        brightnessSlider = [[RSBrightnessSlider alloc] initWithFrame:CGRectMake(165.0, 92.0, 190.0, 30.0)];

        [brightnessSlider setColorPicker:colorPicker];
        brightnessSlider.transform = CGAffineTransformMakeRotation(M_PI_2);
    }
    if (iOSDeviceScreenSize.height == 568){
        //PREPARE NEW VIEW AND PUSH
    
        brightnessSlider = [[RSBrightnessSlider alloc] initWithFrame:CGRectMake(165.0, 132.0, 190.0, 30.0)];
        [brightnessSlider setColorPicker:colorPicker];
        brightnessSlider.transform = CGAffineTransformMakeRotation(M_PI_2);
    }

    btn0.titleLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:18];
    btn1.titleLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:18];
    btn2.titleLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:18];
    btn3.titleLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:18];
    btn4.titleLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:18];
    btn5.titleLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:18];
    btn6.titleLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:18];
    btn7.titleLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:18];
    btn8.titleLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:18];
    btn9.titleLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:18];
    btnA.titleLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:18];
    btnB.titleLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:18];
    btnC.titleLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:18];
    btnD.titleLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:18];
    btnE.titleLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:18];
    btnF.titleLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:18];
    [scrollView1 addSubview:brightnessSlider];
    
    UISwipeGestureRecognizer *swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    swipeRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
    [keyboardView addGestureRecognizer:swipeRecognizer];
    


    
    [btnGenerate addTarget:self
                   action:@selector(getCode:) forControlEvents:UIControlEventTouchDown];

    labelHexText.font = [UIFont fontWithName:@"Montserrat-Bold" size:12];
    uicolorLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:16];

    labelHexCode.font = [UIFont fontWithName:@"Montserrat-Bold" size:45];
    
    textHex.font = [UIFont fontWithName:@"Montserrat-Bold" size:24];
    textHex.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    textHex.autocorrectionType = UITextAutocorrectionTypeNo;
    textHex.delegate = self;

    codeLabel.font = [UIFont fontWithName:@"DejaVu Sans Mono" size:18];

    btnGenerate.titleLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:12];
    btnGenerate.titleLabel.textColor = [UIColor whiteColor];
    btnGenerate.titleLabel.text = @"UICOLOR";
}

- (IBAction)copy:(id)sender{
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    pboard.string = codeLabel.text;
    
    [KGStatusBar showSuccessWithStatus:@"UIColor code copied!"];

}

-(IBAction)goInfo:(id)sender{
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;

    if (iOSDeviceScreenSize.height == 480){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        infoViewController *yourViewController = (infoViewController *)[storyboard instantiateViewControllerWithIdentifier:@"info"];
        
        yourViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:yourViewController animated:YES completion:nil];
    }
    
    if (iOSDeviceScreenSize.height == 568){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"iPhone5" bundle:nil];
        infoViewController *yourViewController = (infoViewController *)[storyboard instantiateViewControllerWithIdentifier:@"info"];
        
        yourViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:yourViewController animated:YES completion:nil];
    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
   secondaryView.frame = CGRectMake(0, primaryView.frame.size.height, secondaryView.frame.size.width, secondaryView.frame.size.height);
    secondaryView.hidden = false;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 6) ? NO : YES;
}

- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"^[0-9A-F]{6}$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    //	return 0;
    return [emailTest evaluateWithObject:candidate];
}

- (IBAction)closeCode:(id)sender
{
    primaryView.userInteractionEnabled=YES;
    
    [UIView animateWithDuration:0.3 animations:^{
        CALayer *layer = primaryView.layer;
        layer.zPosition = -4000;
        CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
        rotationAndPerspectiveTransform.m34 = 1.0 / 300;
        layer.shadowOpacity = 0.01;
        layer.transform = CATransform3DRotate(rotationAndPerspectiveTransform, -10.0f * M_PI / 180.0f, 1.0f, 0.0f, 0.0f);
        
        primaryShadeView.alpha = 0.35;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 animations:^{
            primaryView.transform = CGAffineTransformMakeScale(1.0, 1.0);
            
            primaryShadeView.alpha = 0.0;
            
            secondaryView.frame = CGRectMake(0, primaryView.frame.size.height, secondaryView.frame.size.width, secondaryView.frame.size.height);
        }];
    }];
}


-(void)getCode:(RSColorPickerView *)cp{
    
    //NSLog(@"text: %@", colorString);
    if (keyOpen == YES) {
        [self animateTextField:scrollView1 up:NO];

    }
    
    keyOpen = NO;
    [UIView setAnimationDelegate:self];
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:1.5];
	[UIView setAnimationCurve:UIViewAnimationCurveLinear];
	//	[UIView setAnimationDidStopSelector:self];
	keyboardView.transform =  CGAffineTransformMakeTranslation (0, 568);
	[UIView commitAnimations];
	
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:1.0];
	[UIView setAnimationCurve:UIViewAnimationCurveLinear];
	//	recordTextSecretViewController.view.transform =  CGAffineTransformMakeTranslation (0, 30);
	
	//	[UIView setAnimationDidStopSelector:self];
	//	recordTextSecretViewController.view.transform =  CGAffineTransformMakeTranslation (0, -350);
	[UIView commitAnimations];

    
    if([self validateEmail:[textHex text]] ==1){
        colorString = textHex.text;
        unsigned int colorValueR,colorValueG,colorValueB,colorValueA;
        
        NSString *hexStringCleared = [colorString stringByReplacingOccurrencesOfString:@"#" withString:@""];
        if(hexStringCleared.length == 3) {
            /* short color form */
            /* im lazy, maybe you have a better idea to convert from #fff to #ffffff */
            hexStringCleared = [NSString stringWithFormat:@"%@%@%@%@%@%@", [hexStringCleared substringWithRange:NSMakeRange(0, 1)],[hexStringCleared substringWithRange:NSMakeRange(0, 1)],
                                [hexStringCleared substringWithRange:NSMakeRange(1, 1)],[hexStringCleared substringWithRange:NSMakeRange(1, 1)],
                                [hexStringCleared substringWithRange:NSMakeRange(2, 1)],[hexStringCleared substringWithRange:NSMakeRange(2, 1)]];
        }
        if(hexStringCleared.length == 6) {
            hexStringCleared = [hexStringCleared stringByAppendingString:@"ff"];
        }
        
        /* im in hurry ;) */
        NSString *red = [hexStringCleared substringWithRange:NSMakeRange(0, 2)];
        NSString *green = [hexStringCleared substringWithRange:NSMakeRange(2, 2)];
        NSString *blue = [hexStringCleared substringWithRange:NSMakeRange(4, 2)];
        NSString *alpha = [hexStringCleared substringWithRange:NSMakeRange(6, 2)];
        
        [[NSScanner scannerWithString:red] scanHexInt:&colorValueR];
        [[NSScanner scannerWithString:green] scanHexInt:&colorValueG];
        [[NSScanner scannerWithString:blue] scanHexInt:&colorValueB];
        [[NSScanner scannerWithString:alpha] scanHexInt:&colorValueA];
        
        
        colorPatch.backgroundColor = [UIColor colorWithRed:((colorValueR)&0xFF)/255.0
                                                     green:((colorValueG)&0xFF)/255.0
                                                      blue:((colorValueB)&0xFF)/255.0
                                                     alpha:((colorValueA)&0xFF)/255.0];
        
        
        [colorPicker setSelectionColor:[UIColor colorWithRed:((colorValueR)&0xFF)/255.0
                                                       green:((colorValueG)&0xFF)/255.0
                                                        blue:((colorValueB)&0xFF)/255.0
                                                       alpha:((colorValueA)&0xFF)/255.0]];
        
        
        codeLabel.text = [NSString stringWithFormat:@"[UIColor colorWithRed:%.3f green:%.3f blue:%.3f alpha:%.3f];", ((colorValueR)&0xFF)/255.0, ((colorValueG)&0xFF)/255.0, ((colorValueB)&0xFF)/255.0, ((colorValueA)&0xFF)/255.0];
        
        textHex.text = [NSString stringWithFormat:@"%@", colorString];
        
        [textHex resignFirstResponder];
        
        primaryView.userInteractionEnabled=NO;
        
        [UIView animateWithDuration:0.3 animations:^{
            secondaryView.frame = CGRectMake(0, primaryView.frame.size.height - secondaryView.frame.size.height, secondaryView.frame.size.width, secondaryView.frame.size.height);
            
            CALayer *layer = primaryView.layer;
            layer.zPosition = -4000;
            CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
            rotationAndPerspectiveTransform.m34 = 1.0 / -300;
            layer.shadowOpacity = 0.01;
            layer.transform = CATransform3DRotate(rotationAndPerspectiveTransform, 10.0f * M_PI / 180.0f, 1.0f, 0.0f, 0.0f);
            
            primaryShadeView.alpha = 0.35;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3 animations:^{
                primaryView.transform = CGAffineTransformMakeScale(0.9, 0.9);
                
                primaryShadeView.alpha = 0.5;
            }];
        }];
        [KGStatusBar showSuccessWithStatus:@"HEX color found!"];

    }
    else{
        [textHex becomeFirstResponder];
        textHex.text = @"";
        [KGStatusBar showErrorWithStatus:@"Incorrect HEX Code: Try Again!"];
        self.view.backgroundColor = [UIColor colorWithRed:0.678 green:0.114 blue:0.098 alpha:1];
        [self performSelector:@selector(changeBackground) withObject:nil afterDelay:2.0];

        
    }
    
}

-(void)changeBackground{
    self.view.backgroundColor = [UIColor blackColor];

}

#pragma mark - RSColorPickerView delegate methods

-(void)colorPickerDidChangeSelection:(RSColorPickerView *)cp
{
	colorPatch.backgroundColor = [cp selectionColor];
    
    brightnessSlider.value = [cp brightness];
    //NSLog(@"BRIGHTNESS: %f", [cp brightness]);
    
    NSString *hexString = [NSString stringWithFormat:@"#%02X%02X%02X", (int)((CGColorGetComponents([cp selectionColor].CGColor))[0]*255.0), (int)((CGColorGetComponents([cp selectionColor].CGColor))[1]*255.0), (int)((CGColorGetComponents([cp selectionColor].CGColor))[2]*255.0)];
    //NSLog(@"HEX: %@", hexString);
    
    labelHexCode.text = [NSString stringWithFormat:@"%@", hexString];
    
    hexString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    
    textHex.text = [NSString stringWithFormat:@"%@", hexString];
    
    textHex.placeholder = @"ENTER #HEX";

    //hexLabel.textColor = [cp selectionColor];
    //labelHexCode.font = [UIFont fontWithName:@"Helvetica-Bold" size:22];
    
    //codeLabel.textAlignment = NSTextAlignmentCenter;
    
    
    NSString *stringColor = hexString;
    NSUInteger red;
    NSUInteger green;
    NSUInteger blue;
    sscanf([stringColor UTF8String], "#%02x%02x%02x", &red, &green, &blue);
    
    //RED COLOR
    NSString *redColor = [NSString stringWithFormat: @"%d", red];
    int intRed = [redColor intValue];
    float totalRed = (double)intRed/255;
    //GREEN COLOR
    NSString *greenColor = [NSString stringWithFormat: @"%d", green];
    int intGreen = [greenColor intValue];
    float totalGreen = (double)intGreen/255;
    
    //BLUE COLOR
    NSString *blueColor = [NSString stringWithFormat: @"%d", blue];
    int intBlue = [blueColor intValue];
    float totalBlue = (double)intBlue/255;
    
    if (totalRed == 1.000f && totalGreen == 1.000f && totalBlue == 1.000f ) {
        codeLabel.text = [NSString stringWithFormat:@"[UIColor colorWithRed:%.f green:%.f blue:%.f alpha:%.3f];", totalRed, totalGreen, totalBlue, [cp brightness]];
        //NSLog(@"CODE: [UIColor colorWithRed:%.f green:%.f blue:%.f alpha:%.3f]", totalRed, totalGreen, totalBlue, [cp brightness]);
    }
    else if (totalRed == 1.000f || totalRed == 0.000f) {
        codeLabel.text = [NSString stringWithFormat:@"[UIColor colorWithRed:%.f green:%.3f blue:%.3f alpha:%.3f];", totalRed, totalGreen, totalBlue, [cp brightness]];
        //NSLog(@"CODE: [UIColor colorWithRed:%.f green:%.3f blue:%.3f alpha:%.3f]", totalRed, totalGreen, totalBlue, [cp brightness]);
    }
    
    else if (totalRed == 0.000f && totalGreen == 0.000f && totalBlue == 0.000f ) {
       codeLabel.text = [NSString stringWithFormat:@"[UIColor colorWithRed:%.f green:%.f blue:%.f alpha:%.3f];", totalRed, totalGreen, totalBlue, [cp brightness]];
        //NSLog(@"CODE: [UIColor colorWithRed:%.f green:%.f blue:%.f alpha:%.3f]", totalRed, totalGreen, totalBlue, [cp brightness]);
    }
    else if (totalGreen == 1.000f || totalGreen == 0.000f) {
        codeLabel.text = [NSString stringWithFormat:@"[UIColor colorWithRed:%.3f green:%.f blue:%.3f alpha:%.3f];", totalRed, totalGreen, totalBlue, [cp brightness]];
       // NSLog(@"CODE: [UIColor colorWithRed:%.3f green:%.f blue:%.3f alpha:%.3f]", totalRed, totalGreen, totalBlue, [cp brightness]);
    }
    else if (totalBlue == 1.000f || totalBlue == 0.000f) {
        codeLabel.text = [NSString stringWithFormat:@"[UIColor colorWithRed:%.3f green:%.3f blue:%.f alpha:%.3f];", totalRed, totalGreen, totalBlue, [cp brightness]];
       // NSLog(@"CODE: [UIColor colorWithRed:%.3f green:%.3f blue:%.f alpha:%.3f]", totalRed, totalGreen, totalBlue, [cp brightness]);
    }
    else{
        //NSLog(@"CODE: [UIColor colorWithRed:%.3f green:%.3f blue:%.3f alpha:%.3f]", totalRed, totalGreen, totalBlue, [cp brightness]);
        
        codeLabel.text = [NSString stringWithFormat:@"[UIColor colorWithRed:%.3f green:%.3f blue:%.3f alpha:%.3f];", totalRed, totalGreen, totalBlue, [cp brightness]];
    }
    
    
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    [NSKeyedArchiver archiveRootObject:colorPicker toFile:savePath];
}


-(IBAction)editTextfield:(id)sender{
    //scrollView1.contentOffset = CGPointMake(0,90);
    [self slideKeyboard];

    if (keyOpen == NO) {
  
    [self animateTextField:scrollView1 up:YES];
        textHex.text = nil;
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return NO;  // Hide both keyboard and blinking cursor.
}


-(void)slideKeyboard{
    
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    
    if (iOSDeviceScreenSize.height == 480){
        [UIView setAnimationDelegate:self];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        //	[UIView setAnimationDidStopSelector:self];
        keyboardView.transform =  CGAffineTransformMakeTranslation (0, -295);
        [UIView commitAnimations];
        
        CABasicAnimation *bounceAnimation = [CABasicAnimation animationWithKeyPath:@"position.y"];
        bounceAnimation.duration = 0.2;
        bounceAnimation.fromValue = [NSNumber numberWithInt:0];
        bounceAnimation.toValue = [NSNumber numberWithInt:10];
        bounceAnimation.repeatCount = 2;
        bounceAnimation.autoreverses = YES;
        bounceAnimation.fillMode = kCAFillModeForwards;
        bounceAnimation.removedOnCompletion = NO;
        bounceAnimation.additive = YES;
        [keyboardView.layer addAnimation:bounceAnimation forKey:@"bounceAnimation"];
        
        
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:1.0];
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        
        [UIView commitAnimations];
        
    }
    
    if (iOSDeviceScreenSize.height == 568){
        [UIView setAnimationDelegate:self];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.6];
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        //	[UIView setAnimationDidStopSelector:self];
        keyboardView.transform =  CGAffineTransformMakeTranslation (0, -295);
        [UIView commitAnimations];
        
        CABasicAnimation *bounceAnimation = [CABasicAnimation animationWithKeyPath:@"position.y"];
        bounceAnimation.duration = 0.2;
        bounceAnimation.fromValue = [NSNumber numberWithInt:0];
        bounceAnimation.toValue = [NSNumber numberWithInt:10];
        bounceAnimation.repeatCount = 2;
        bounceAnimation.autoreverses = YES;
        bounceAnimation.fillMode = kCAFillModeForwards;
        bounceAnimation.removedOnCompletion = NO;
        bounceAnimation.additive = YES;
        [keyboardView.layer addAnimation:bounceAnimation forKey:@"bounceAnimation"];
        
        
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:1.0];
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        
        [UIView commitAnimations];

    }

    
}
- (void)dismissKeyboard:(UITapGestureRecognizer *)recognizer{
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    
    if (iOSDeviceScreenSize.height == 480){
        
        [self animateTextField:scrollView1 up:NO];
        
        keyOpen = NO;
        [UIView setAnimationDelegate:self];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:1.5];
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        //	[UIView setAnimationDidStopSelector:self];
        keyboardView.transform =  CGAffineTransformMakeTranslation (0, 480);
        [UIView commitAnimations];
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:1.0];
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        //	recordTextSecretViewController.view.transform =  CGAffineTransformMakeTranslation (0, 30);
        
        //	[UIView setAnimationDidStopSelector:self];
        //	recordTextSecretViewController.view.transform =  CGAffineTransformMakeTranslation (0, -350);
        [UIView commitAnimations];

    }
    
    if (iOSDeviceScreenSize.height == 568){
        [self animateTextField:scrollView1 up:NO];
        
        keyOpen = NO;
        [UIView setAnimationDelegate:self];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:1.5];
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        //	[UIView setAnimationDidStopSelector:self];
        keyboardView.transform =  CGAffineTransformMakeTranslation (0, 568);
        [UIView commitAnimations];
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:1.0];
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        //	recordTextSecretViewController.view.transform =  CGAffineTransformMakeTranslation (0, 30);
        
        //	[UIView setAnimationDidStopSelector:self];
        //	recordTextSecretViewController.view.transform =  CGAffineTransformMakeTranslation (0, -350);
        [UIView commitAnimations];

        
    }
}

- (void) animateTextField: (UIScrollView*) scrollView up: (BOOL) up
{
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    
    if (iOSDeviceScreenSize.height == 480){
        
        keyOpen = YES;
        
        int animatedDistance;
        
        int moveUpValue = scrollView1.contentOffset.y+ scrollView1.frame.size.height;
        
        NSLog(@"Y: %f", scrollView1.contentOffset.y);
        
        animatedDistance = 270-(460-moveUpValue-5);
        
        
        if(animatedDistance>0)
        {
            const int movementDistance = animatedDistance;
            const float movementDuration = 0.3f;
            int movement = (up ? -movementDistance : movementDistance);
            [UIView beginAnimations: nil context: nil];
            [UIView setAnimationBeginsFromCurrentState: YES];
            [UIView setAnimationDuration: movementDuration];
            scrollView1.frame = CGRectOffset(scrollView1.frame, 0, movement);
            
            [UIView commitAnimations];
            NSLog(@"x: %f", scrollView1.contentOffset.y);
            
        }

    }
    
    if (iOSDeviceScreenSize.height == 568){
        keyOpen = YES;
        
        int animatedDistance;
        
        int moveUpValue = scrollView1.contentOffset.y+ scrollView1.frame.size.height;
        
        NSLog(@"Y: %f", scrollView1.contentOffset.y);
        
        animatedDistance = 185-(460-moveUpValue-5);
        
        
        if(animatedDistance>0)
        {
            const int movementDistance = animatedDistance;
            const float movementDuration = 0.3f;
            int movement = (up ? -movementDistance : movementDistance);
            [UIView beginAnimations: nil context: nil];
            [UIView setAnimationBeginsFromCurrentState: YES];
            [UIView setAnimationDuration: movementDuration];
            scrollView1.frame = CGRectOffset(scrollView1.frame, 0, movement);
            
            [UIView commitAnimations];
            NSLog(@"x: %f", scrollView1.contentOffset.y);
            
        }

        
    }

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btnPressed1:(id)sender{

    if ([textHex.text length] <= 0){
        textHex.text = [NSString stringWithFormat:@"1"];

    }
    else{
        textHex.text = [NSString stringWithFormat:@"%@1", textHex.text];
    }
    
}
-(IBAction)btnPressed2:(id)sender{
    if ([textHex.text length] <= 0){
        textHex.text = [NSString stringWithFormat:@"2"];
        
    }
    else{
        textHex.text = [NSString stringWithFormat:@"%@2", textHex.text];
    }

}
-(IBAction)btnPressed3:(id)sender{
    if ([textHex.text length] <= 0){
        textHex.text = [NSString stringWithFormat:@"3"];
        
    }
    else{
        textHex.text = [NSString stringWithFormat:@"%@3", textHex.text];
    }

}
-(IBAction)btnPressed4:(id)sender{
    if ([textHex.text length] <= 0){
        textHex.text = [NSString stringWithFormat:@"4"];
        
    }
    else{
        textHex.text = [NSString stringWithFormat:@"%@4", textHex.text];
    }
}
-(IBAction)btnPressed5:(id)sender{
    if ([textHex.text length] <= 0){
        textHex.text = [NSString stringWithFormat:@"5"];
        
    }
    else{
        textHex.text = [NSString stringWithFormat:@"%@5", textHex.text];
    }

}
-(IBAction)btnPressed6:(id)sender{
    if ([textHex.text length] <= 0){
        textHex.text = [NSString stringWithFormat:@"6"];
        
    }
    else{
        textHex.text = [NSString stringWithFormat:@"%@6", textHex.text];
    }

}
-(IBAction)btnPressed7:(id)sender{
    if ([textHex.text length] <= 0){
        textHex.text = [NSString stringWithFormat:@"7"];
        
    }
    else{
        textHex.text = [NSString stringWithFormat:@"%@7", textHex.text];
    }

}
-(IBAction)btnPressed8:(id)sender{
    if ([textHex.text length] <= 0){
        textHex.text = [NSString stringWithFormat:@"8"];
        
    }
    else{
        textHex.text = [NSString stringWithFormat:@"%@8", textHex.text];
    }

}
-(IBAction)btnPressed9:(id)sender{
    if ([textHex.text length] <= 0){
        textHex.text = [NSString stringWithFormat:@"9"];
        
    }
    else{
        textHex.text = [NSString stringWithFormat:@"%@9", textHex.text];
    }

}
-(IBAction)btnPressed0:(id)sender{
    if ([textHex.text length] <= 0){
        textHex.text = [NSString stringWithFormat:@"0"];
        
    }
    else{
        textHex.text = [NSString stringWithFormat:@"%@0", textHex.text];
    }

}

-(IBAction)btnPressedA:(id)sender{
    if ([textHex.text length] <= 0){
        textHex.text = [NSString stringWithFormat:@"A"];
        
    }
    else{
        textHex.text = [NSString stringWithFormat:@"%@A", textHex.text];
    }

}
-(IBAction)btnPressedB:(id)sender{
    if ([textHex.text length] <= 0){
        textHex.text = [NSString stringWithFormat:@"B"];
        
    }
    else{
        textHex.text = [NSString stringWithFormat:@"%@B", textHex.text];
    }

}
-(IBAction)btnPressedC:(id)sender{
    if ([textHex.text length] <= 0){
        textHex.text = [NSString stringWithFormat:@"C"];
        
    }
    else{
        textHex.text = [NSString stringWithFormat:@"%@C", textHex.text];
    }

}
-(IBAction)btnPressedD:(id)sender{
    if ([textHex.text length] <= 0){
        textHex.text = [NSString stringWithFormat:@"D"];
        
    }
    else{
        textHex.text = [NSString stringWithFormat:@"%@D", textHex.text];
    }

}
-(IBAction)btnPressedE:(id)sender{
    if ([textHex.text length] <= 0){
        textHex.text = [NSString stringWithFormat:@"E"];
        
    }
    else{
        textHex.text = [NSString stringWithFormat:@"%@E", textHex.text];
    }

}
-(IBAction)btnPressedF:(id)sender{
    if ([textHex.text length] <= 0){
        textHex.text = [NSString stringWithFormat:@"F"];
        
    }
    else{
        textHex.text = [NSString stringWithFormat:@"%@F", textHex.text];
    }

}
-(IBAction)backspace:(id)sender{
     if ([textHex.text length] > 0){
    textHex.text = [textHex.text substringToIndex:[textHex.text length]-1];
     }

}
@end
