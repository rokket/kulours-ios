//
//  introViewController.h
//  Kulours
//
//  Created by Hassan Mahmood on 11/05/2013.
//  Copyright (c) 2013 Hassan Mahmood. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface introViewController : UIViewController<UIScrollViewDelegate>{
    UIScrollView* scrollView;
	UIPageControl* pageControl;
	
	BOOL pageControlBeingUsed;
}

@property (nonatomic, retain) IBOutlet UIButton *doneButton;

@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UILabel *detailLabel;

@property (nonatomic, retain) IBOutlet UILabel *title2Label;
@property (nonatomic, retain) IBOutlet UILabel *detail2Label;

@property (nonatomic, retain) IBOutlet UILabel *title3Label;
@property (nonatomic, retain) IBOutlet UILabel *detail3Label;

@property (nonatomic, retain) IBOutlet UILabel *title4Label;
@property (nonatomic, retain) IBOutlet UILabel *detail4Label;

@property (nonatomic, retain) IBOutlet UILabel *title5Label;
@property (nonatomic, retain) IBOutlet UILabel *detail5Label;

@property (nonatomic, retain) IBOutlet UILabel *extraLabel;

@property (nonatomic, retain) IBOutlet UIScrollView* scrollView;
@property (nonatomic, retain) IBOutlet UIPageControl* pageControl;

- (IBAction)changePage:(id)sender;
- (IBAction)done:(id)sender;

@end
