//
//  infoViewController.h
//  Kulours
//
//  Created by Hassan Mahmood on 10/05/2013.
//  Copyright (c) 2013 Hassan Mahmood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface infoViewController : UIViewController <MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>
@property (nonatomic, retain) IBOutlet UITableView *tableView;

@property (nonatomic, retain) IBOutlet UIView *keyboardView;
-(IBAction)goBack:(id)sender;

-(IBAction)slide:(id)sender;
-(IBAction)dismissKeyboard:(id)sender;
- (IBAction)push:(id)sender;
-(void)changeBackground;
@end
