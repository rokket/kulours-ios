//
//  tutorialViewController.m
//  Kulours
//
//  Created by Hassan Mahmood on 11/05/2013.
//  Copyright (c) 2013 Hassan Mahmood. All rights reserved.
//

#import "introViewController.h"
#import "ViewController.h"
#import "AppDelegate.h"
@interface introViewController ()

@end

@implementation introViewController
@synthesize titleLabel, detailLabel, scrollView, pageControl, title2Label, title3Label, title4Label, title5Label, detail2Label, detail3Label, detail4Label, detail5Label, extraLabel, doneButton;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    pageControlBeingUsed = NO;
	
	self.scrollView.contentSize = CGSizeMake(1600, 329);
    [super viewDidLoad];
    scrollView.delegate = self;
    
    [self.pageControl setNumberOfPages:5];
    titleLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:20];
    detailLabel.font = [UIFont fontWithName:@"Montserrat-Regular" size:15];
    doneButton.titleLabel.font = [UIFont fontWithName:@"Montserrat-Bold" size:20];
    
    title2Label.font = [UIFont fontWithName:@"Montserrat-Bold" size:20];
    detail2Label.font = [UIFont fontWithName:@"Montserrat-Regular" size:15];
    title3Label.font = [UIFont fontWithName:@"Montserrat-Bold" size:20];
    detail3Label.font = [UIFont fontWithName:@"Montserrat-Regular" size:15];
    title4Label.font = [UIFont fontWithName:@"Montserrat-Bold" size:20];
    detail4Label.font = [UIFont fontWithName:@"Montserrat-Regular" size:15];
    title5Label.font = [UIFont fontWithName:@"Montserrat-Bold" size:16];
    detail5Label.font = [UIFont fontWithName:@"Montserrat-Regular" size:15];
    
    extraLabel.font = [UIFont fontWithName:@"Montserrat-Regular" size:13];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FirstLaunch"])
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 480){
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            ViewController *yourViewController = (ViewController *)[storyboard instantiateViewControllerWithIdentifier:@"home"];
            
            
            [self.navigationController pushViewController:yourViewController animated:YES];
        }
        
        if (iOSDeviceScreenSize.height == 568){
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"iPhone5" bundle:nil];
            ViewController *yourViewController = (ViewController *)[storyboard instantiateViewControllerWithIdentifier:@"home"];
            
            [self.navigationController pushViewController:yourViewController animated:YES];
            
        }
    }
    else {
        // Place code here
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FirstLaunch"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}



- (void)scrollViewDidScroll:(UIScrollView *)sender {
	if (!pageControlBeingUsed) {
		// Switch the indicator when more than 50% of the previous/next page is visible
		CGFloat pageWidth = self.scrollView.frame.size.width;
		int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
		self.pageControl.currentPage = page;
	}
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	pageControlBeingUsed = NO;
}

- (IBAction)changePage:(id)sender{
	// Update the scroll view to the appropriate page
	CGRect frame;
	frame.origin.x = self.scrollView.frame.size.width * self.pageControl.currentPage;
	frame.origin.y = 0;
	frame.size = self.scrollView.frame.size;
	[self.scrollView scrollRectToVisible:frame animated:YES];
	
	pageControlBeingUsed = YES;
}

- (IBAction)done:(id)sender{
    
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    
    if (iOSDeviceScreenSize.height == 480){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        ViewController *yourViewController = (ViewController *)[storyboard instantiateViewControllerWithIdentifier:@"home"];
        
        
        [self.navigationController pushViewController:yourViewController animated:YES];
    }
    
    if (iOSDeviceScreenSize.height == 568){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"iPhone5" bundle:nil];
        ViewController *yourViewController = (ViewController *)[storyboard instantiateViewControllerWithIdentifier:@"home"];
        
        [self.navigationController pushViewController:yourViewController animated:YES];

    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
