//
//  ViewController.h
//  Kulours
//
//  Created by Hassan Mahmood on 07/05/2013.
//  Copyright (c) 2013 Hassan Mahmood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSColorPickerView.h"
@class RSBrightnessSlider;
@class TPKeyboardAvoidingScrollView;

@interface ViewController : UIViewController <RSColorPickerViewDelegate, UITextFieldDelegate>{
    NSString * savePath;
    NSString *colorString;
    NSCharacterSet *myCharSet;
    BOOL keyOpen;
    IBOutlet UIButton *btn1,*btn2,*btn3,*btn4,*btn5,*btn6,*btn7,*btn8,*btn9,*btn0,*btnA,*btnB,*btnC,*btnD,*btnE,*btnF;

}

@property (nonatomic, retain) IBOutlet UILabel *labelHexCode;
@property (nonatomic, retain) IBOutlet UILabel *labelHexText;
@property (nonatomic, retain) IBOutlet UILabel *codeLabel;
@property (nonatomic, retain) IBOutlet UILabel *uicolorLabel;
@property (nonatomic, retain) IBOutlet UIView *keyboardView;

@property (nonatomic, retain) IBOutlet UITextField *textHex;
@property (nonatomic, retain) IBOutlet UIButton *btnGenerate;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView1;
@property (nonatomic, retain) IBOutlet RSColorPickerView *colorPicker;
@property (nonatomic, retain) IBOutlet RSBrightnessSlider *brightnessSlider;
@property (nonatomic, retain) IBOutlet UIView *colorPatch;
@property (retain, nonatomic) IBOutlet UIView *primaryView;
@property (retain, nonatomic) IBOutlet UIView *secondaryView;
@property (retain, nonatomic) IBOutlet UIView *primaryShadeView;
- (IBAction)closeCode:(id)sender;
- (IBAction)copy:(id)sender;
-(IBAction)goInfo:(id)sender;
-(IBAction)btnPressed1:(id)sender;
-(IBAction)btnPressed2:(id)sender;
-(IBAction)btnPressed3:(id)sender;
-(IBAction)btnPressed4:(id)sender;
-(IBAction)btnPressed5:(id)sender;
-(IBAction)btnPressed6:(id)sender;
-(IBAction)btnPressed7:(id)sender;
-(IBAction)btnPressed8:(id)sender;
-(IBAction)btnPressed9:(id)sender;
-(IBAction)btnPressed0:(id)sender;

-(IBAction)btnPressedA:(id)sender;
-(IBAction)btnPressedB:(id)sender;
-(IBAction)btnPressedC:(id)sender;
-(IBAction)btnPressedD:(id)sender;
-(IBAction)btnPressedE:(id)sender;
-(IBAction)btnPressedF:(id)sender;
-(IBAction)backspace:(id)sender;


-(void)slideKeyboard;
- (void)dismissKeyboard:(UITapGestureRecognizer *)recognizer;
-(IBAction)editTextfield:(id)sender;

-(void)getCode:(RSColorPickerView *)cp;
- (BOOL) validateEmail: (NSString *) candidate;
-(void)changeBackground;
@end
