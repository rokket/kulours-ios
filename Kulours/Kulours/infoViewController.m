//
//  infoViewController.m
//  Kulours
//
//  Created by Hassan Mahmood on 10/05/2013.
//  Copyright (c) 2013 Hassan Mahmood. All rights reserved.
//

#import "infoViewController.h"
#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "tutorialViewController.h"
#import "CheckForUpdate.h"
#import "KGStatusBar.h"

@interface infoViewController ()

@end

@implementation infoViewController
@synthesize keyboardView, tableView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)goBack:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
}

- (IBAction)push:(id)sender{
    
    
       
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (NSInteger)tableView:(UITableView *)table
 numberOfRowsInSection:(NSInteger)section {
    if(section == 0)
        return 4;
    else
        return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *SimpleTableIdentifier = @"SimpleTableIdentifier";
    
    
    UITableViewCell * cell = [self.tableView
                              dequeueReusableCellWithIdentifier: SimpleTableIdentifier];
    
    if(cell == nil) {
        
    cell = [[UITableViewCell alloc]
                 initWithStyle:UITableViewCellStyleDefault
                 reuseIdentifier:SimpleTableIdentifier];
        
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    self.tableView.separatorColor = [UIColor blackColor];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    NSUInteger row = [indexPath row];
    NSUInteger section = [indexPath section];

    cell.textLabel.font = [UIFont fontWithName:@"Montserrat-Regular" size:15];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (section ==0) {
  
    if (row == 0) {
        cell.textLabel.text = @"Tutorial";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    }
    if (row == 1) {
        cell.textLabel.text = @"Rate Kulours";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    }
    if (row == 2) {
        cell.textLabel.text = @"Check For Update";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    }
    if (row == 3) {
        cell.textLabel.text = [NSString stringWithFormat:@"Version %@ ", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
        [cell setUserInteractionEnabled:NO];

    }
    }
    if (section ==1) {
        
        if (row == 0) {
            cell.textLabel.text = @"Follow Developer on Twitter";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
        }
        if (row == 1) {
            cell.textLabel.text = @"Email Developer";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
        }
    }
    

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger row = [indexPath row];
    NSUInteger section = [indexPath section];

    
    if (section ==0) {

    if (row == 0) {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 480){
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            tutorialViewController *yourViewController = (tutorialViewController *)[storyboard instantiateViewControllerWithIdentifier:@"tut"];
            
            [self presentViewController:yourViewController animated:YES completion:nil];
        }
        
        if (iOSDeviceScreenSize.height == 568){
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"iPhone5" bundle:nil];
            tutorialViewController *yourViewController = (tutorialViewController *)[storyboard instantiateViewControllerWithIdentifier:@"tut"];
            
            [self presentViewController:yourViewController animated:YES completion:nil];
            
        }
        
    }
    if (row == 1) {
        
        NSString *str = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa";
        str = [NSString stringWithFormat:@"%@/wa/viewContentsUserReviews?", str];
        str = [NSString stringWithFormat:@"%@type=Purple+Software&id=", str];
        
        // Here is the app id from itunesconnect
        str = [NSString stringWithFormat:@"%@645487308", str];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
        
    }
    if (row == 2) {
        [CheckForUpdate version];

    }
    if (row == 3) {

        }
    
    }
    
    if (section ==1) {
        
        if (row == 0) {

            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.twitter.com/hassanmahmood_"]];
            
        }
        if (row == 1) {
            if (![MFMailComposeViewController canSendMail]){
                [KGStatusBar showErrorWithStatus:@"No Email Account Found!"];
                self.view.backgroundColor = [UIColor colorWithRed:0.678 green:0.114 blue:0.098 alpha:1];
                [self performSelector:@selector(changeBackground) withObject:nil afterDelay:3.0];
            }
            else{
            MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
            picker.mailComposeDelegate = self;
            
            NSArray *torec=[[NSArray alloc] initWithObjects:@"me@hassanmahmood.co.uk", nil];
            [picker setToRecipients:torec];
            [picker setSubject:@"Kulours App"];
            [self presentModalViewController:picker animated:YES];
            }
            
        }
                
    }
}

-(void)changeBackground{
    self.view.backgroundColor = [UIColor blackColor];
    
}


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            [KGStatusBar showErrorWithStatus:@"Cancelled!"];
            self.view.backgroundColor = [UIColor colorWithRed:0.678 green:0.114 blue:0.098 alpha:1];
            [self performSelector:@selector(changeBackground) withObject:nil afterDelay:2.0];
            break;
        case MFMailComposeResultSaved:
            [KGStatusBar showSuccessWithStatus:@"Email saved!"];

            break;
        case MFMailComposeResultSent:
            [KGStatusBar showSuccessWithStatus:@"Email sent successfully!"];

            break;
        case MFMailComposeResultFailed:
            [KGStatusBar showErrorWithStatus:@"Sending failed!"];
            self.view.backgroundColor = [UIColor colorWithRed:0.678 green:0.114 blue:0.098 alpha:1];
            [self performSelector:@selector(changeBackground) withObject:nil afterDelay:2.0];
            break;
        default:
            [KGStatusBar showErrorWithStatus:@"Not sent!"];
            self.view.backgroundColor = [UIColor colorWithRed:0.678 green:0.114 blue:0.098 alpha:1];
            [self performSelector:@selector(changeBackground) withObject:nil afterDelay:2.0];
            break;
    }
    
    [self dismissModalViewControllerAnimated:YES];
}


-(IBAction)slide:(id)sender{
	
}

-(IBAction)dismissKeyboard:(id)sender{

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
