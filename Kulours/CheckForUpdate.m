//
//  CheckForUpdate.m
//  
//
//  Created by Hassan Mahmood on 02/02/2013.
//
// ******************************
// ********* HOW TO ADD *********
// ******************************
// #import "CheckForUpdate.h" to AppDelegate.m
// in applicationDidFinishLaunching add [CheckForUpdate version];

#import "CheckForUpdate.h"
#import "KGStatusBar.h"
#import "infoViewController.h"


#define kAppleID @"645487308" //found in iTunes connect under Apple ID typically like 573293275
#define kCurrentVersion [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleVersionKey]

@interface CheckForUpdate ()

+ (void)showAlert:(NSString*)appStoreVersion;

@end

@implementation CheckForUpdate

+ (void)version
{
    
    // Asynchronously query iTunes AppStore for publically available version
    NSString *storeString = [NSString stringWithFormat:@"http://itunes.apple.com/lookup?id=%@", kAppleID];
    NSURL *storeURL = [NSURL URLWithString:storeString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:storeURL];
    [request setHTTPMethod:@"GET"];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if ( [data length] > 0 && !error ) { // Success
            
            NSDictionary *appData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                // All versions that have been uploaded to the AppStore
                NSArray *versionsInAppStore = [[appData valueForKey:@"results"] valueForKey:@"version"];
                
                if ( ![versionsInAppStore count] ) { // No versions of app in AppStore
                    
                    infoViewController *info = [[infoViewController alloc] init];
                    [KGStatusBar showErrorWithStatus:@"App not found!"];
                    info.view.backgroundColor = [UIColor colorWithRed:0.678 green:0.114 blue:0.098 alpha:1];
                    [self performSelector:@selector(backgroundColor) withObject:nil afterDelay:2.0];
                } else {
                    
                    NSString *currentAppStoreVersion = [versionsInAppStore objectAtIndex:0];
                    
                    if ([kCurrentVersion compare:currentAppStoreVersion options:NSNumericSearch] == NSOrderedAscending) {
		                
                        NSString *iTunesString = [NSString stringWithFormat:@"https://itunes.apple.com/app/id%@", kAppleID];
                        NSURL *iTunesURL = [NSURL URLWithString:iTunesString];
                        [[UIApplication sharedApplication] openURL:iTunesURL];
                    }
                    else {
                        infoViewController *info = [[infoViewController alloc] init];
                        [KGStatusBar showErrorWithStatus:@"No update found!"];
                        info.view.backgroundColor = [UIColor colorWithRed:0.678 green:0.114 blue:0.098 alpha:1];
                        [self performSelector:@selector(backgroundColor) withObject:nil afterDelay:2.0];
                        // Current installed version is the newest public version or newer
                        
                        }
                    
                }
                
            });
        }
        
    }];
}


+(void)backgroundColor{
    infoViewController *info = [[infoViewController alloc] init];

    info.view.backgroundColor = [UIColor blackColor];
    
}

#pragma mark - Private Methods
+ (void)showAlert:(NSString *)currentAppStoreVersion
{
    
    NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
    
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"New Update"
                                                            message:[NSString stringWithFormat:@"A new version of %@ is available. Please update to version %@ now.", appName, currentAppStoreVersion]
                                                           delegate:self
                                                  cancelButtonTitle:@"Later"
                                                  otherButtonTitles:@"Update Now", nil];
        
        [alertView show];
        
    
    
}

#pragma mark - UIAlertViewDelegate Methods
+ (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
        switch ( buttonIndex ) {
                
            case 0:{ // Cancel / Not now
                
                // Do nothing
                
            } break;
                
            case 1:{ // Update
                
                NSString *iTunesString = [NSString stringWithFormat:@"https://itunes.apple.com/app/id%@", kAppleID];
                NSURL *iTunesURL = [NSURL URLWithString:iTunesString];
                [[UIApplication sharedApplication] openURL:iTunesURL];
                
            } break;
                
            default:
                break;
        }
        
    
    
    
}



@end
